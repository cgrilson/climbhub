# ClimbHub
#### Notice: app currently down while we develop a strategy for server costs
![](https://publicdomainvectors.org/photos/escalade.png)

Tracking your climbs and engaging in some friendly competition - at your local gym.

Built with R Shiny & Javascript, with connections to a MySQL database (on Amazon RDS), running in a Docker container on an AWS EC2 instance. User authentication with Auth0. 

